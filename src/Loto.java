import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSpan;
import com.gargoylesoftware.htmlunit.html.HtmlTable;
import com.gargoylesoftware.htmlunit.html.HtmlTableCell;
import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;   
import java.util.ArrayList;
import java.util.List;


public class Loto {
    public static void main(String[] args) throws IOException {
        
      java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(java.util.logging.Level.OFF); 
      java.util.logging.Logger.getLogger("org.apache.http").setLevel(java.util.logging.Level.OFF);  
      
      CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
      String URL = "http://www.loterias.caixa.gov.br/wps/portal/loterias/landing/lotofacil";
      WebClient webClient = new WebClient(BrowserVersion.getDefault());
      HtmlPage page = webClient.getPage(URL);
      
      webClient.getOptions().setJavaScriptEnabled(true);

      webClient.waitForBackgroundJavaScript(9000);
      
      System.out.println(page.getTitleText());
      System.out.println(page.getUrl().toString());
            
      List<HtmlElement> spans = page.getBody().getElementsByAttribute("span", "class", "ng-binding");
      HtmlSpan concurso = (HtmlSpan) spans.get(0);
      String ccs = concurso.getTextContent().replaceAll("\\s+", " ").trim();
      System.out.println(ccs);
      
      List<Long> resultados = new ArrayList();  
      List<HtmlElement> lista = page.getBody().getElementsByAttribute("table", "class", "simple-table lotofacil"); 
      HtmlTable tabela = (HtmlTable) lista.get(0);
      List<HtmlTableCell> cells = tabela.getBodies().get(0).getElementsByAttribute("td", "ng-repeat", "dezena in resultadoLinha");
      cells.forEach(cell -> resultados.add(Long.parseLong(cell.getTextContent())));
      System.out.println(resultados); 
    }
}

